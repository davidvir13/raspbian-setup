SH_ALIAS=.bash_aliases

if [ ! -f "SH_ALIAS" ]; then
  cp $SH_ALIAS ~/$SH_ALIAS
fi

#sudo apt-get update -y --force-yes  && sudo apt-get upgrade -y --force-yes

# Real VNC
sudo apt-get install realvnc-vnc-server 
# realvnc-vnc-viewer

# Libre Office
#sudo apt-get -yes --force-yes install libreoffice 

# HP Printer
#sudo apt-get -yes --force-yes install hplip-gui
#sudo hp-setup

# Torrent client
#sudo apt-get -yes --force-yes install deluge

# youtube-dl
#sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
#sudo chmod a+rx /usr/local/bin/youtube-dl

# Anime-Downloader
#pip install anime-downloader

# Gparted
#sudo apt-get -yes --force-yes install gparted

# Mp3 Edittor
#sudo apt-get -yes --force-yes install audacity
