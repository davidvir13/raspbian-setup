#!/bin/bash -xv

SH_ALIAS=bash_aliases
RC_CONKY=conkyrc
SH_CONKY=conky.sh
DT_CONKY=conky.desktop


copying_file(){
 SOURCE=$1
 DESTINATION=$2
 #if [ ! -f $SOURCE ]; then
  echo "copying_file $SOURCE $DESTINATION"
  sudo cp $SOURCE $DESTINATION
 #fi
}


sudo apt-get update -y  && sudo apt-get upgrade -y 
copying_file _$SH_ALIAS ~/.$SH_ALIAS


# Real VNC
sudo apt-get install realvnc-vnc-server -y 
realvnc-vnc-viewer


# Conky 
sudo apt-get install -y conky 
copying_file $SH_CONKY /usr/bin/$SH_CONKY
copying_file _$RC_CONKY ~/.$RC_CONKY
copying_file $DT_CONKY /etc/xdg/autostart/$DT_CONKY 

# youtube-dl
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl


# Anime-Downloader
#sudo apt-get -y install nodejs
#sudo pip3 install anime-downloader[cloudflare] cfscrape


# Torrent client
sudo apt-get -y install deluge
sudo service deluged start


# Gparted
#sudo apt-get -y install gparted


# Mac Changer
sudo apt-get -y install macchanger nmap


# HP Printer
#sudo apt-get -y install hplip-gui
#sudo hp-setup


# Libre Office
#sudo apt-get -y install libreoffice 


# Mp3 Edittor
#sudo apt-get -y install audacity

